# NetBASILISK 

# Overview 
The NetBASILISK (Network Border at Scale Integrating and Leveraging Individual Security Components) project aims to deploy and produce a network border security 
solution that provides a more efficient security monitoring infrastructure and monitoring software to protect resources without impacting scientific research and 
collaboration. The main motivation behind the project was to produce an alternative solution to previous tools such as the 
firewall and Science DMZs. The NetBASILISK project has three main science drivers:

* **Physics** 
* Cryo-Electron Microscopy (Cryo-EM)
* Electrical Engineering and Computer Science (EECS)

In Physics, the use-case scenario would be receiving large data from external collaborators (e.g CERN and BNL). This repository is dedicated to building a 
benchmark for network testing in to understand the impact of NetBASILISK when importing ATLAS data to the AGLT2 UM Cluster. The benchmark has to be reproducible
in order to evaluate the performace of importing data to AGLT2. 

# Requirements 
### **For Physics users:**

**Grid Certificate**<br/> 
The script requires using grid resources. To gain access, you have to obtain a grid certificate. Instructions here: https://www.racf.bnl.gov/docs/howto/grid/getcert 

**Virtual Organization (VO)** <br/>
After obtaining a grid certificate, you need to become a member of a virtual organization (VO) and add your grid certificate to the membership list
on a VOMS (Virtual Organization Management System) server. Instructions here: https://www.racf.bnl.gov/docs/howto/grid/joinvo

**Using the Grid**  <br/>
Install the grid certificate in your home directory and web browser. Instructions here: https://www.racf.bnl.gov/docs/howto/grid/installcert

**Establish the environment**  <br/>
Setup for the environment depends on the site or experiment. For ATLAS,
```
export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
alias setupATLAS="source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
setupATLAS
lsetup rucio 
```

**VOMS Proxy** <br/>
```
voms-proxy-init -voms atlas <<!
<passwd>
!
```

### **For Non-Physics users:**
 
This section would give an overview in installing the Globus Command Line Interface (CLI). More information: https://docs.globus.org/cli/installation/

**Prerequisites** <br/>
More information: https://docs.globus.org/cli/installation/prereqs/
* Python <br/>
Globus CLI  requires Python 2.7+ or 3.4+. To check, in terminal 
```
python --version 
```
If the command above does not give the version information or a "command not found" error, then you will have to install one. Please visit https://www.python.org/downloads/ to get the
latest or appropriate python installer for your operating system 

* Pip <br/>
To check if pip is installed, in terminal 
```
pip --version
```
If the command above does not give the version information or a "command not found" error, please refer to the website in under **Prerequisites**

* Add pip --user installs to your PATH <br/>
The final requirement for installing the CLI is to add the pip --user install directory to your shell’s PATH variable. (These instructions are relevant on Linux and
macOS.) Add the following lines to your .bashrc
```
GLOBUS_CLI_INSTALL_DIR="$(python -c 'import site; print(site.USER_BASE)')/bin"
echo "GLOBUS_CLI_INSTALL_DIR=$GLOBUS_CLI_INSTALL_DIR"

export PATH="$GLOBUS_CLI_INSTALL_DIR:$PATH"
echo 'export PATH="'"$GLOBUS_CLI_INSTALL_DIR"':$PATH"' >> "$HOME/.bashrc"
```
**Install the CLI** <br/>
More information: https://docs.globus.org/cli/installation/. To install of the CLI, run the following command:
```
pip install --upgrade --user globus-cli
```
To check if the CLI is installed:
```
globus --help
```
Login to Globus CLI requires authentication to Globus services
```
globus login
# follow instructions to get set up
```
After authentication and login, the syntax for a simple transfer: (https://docs.globus.org/cli/reference/transfer/)
```
globus transfer ['OPTIONS']—SOURCE_ENDPOINT_ID:SOURCE_PATH DEST_ENDPOINT_ID:DEST_PATH
```
To update your version of the CLI to the latest:
```
globus update
```
To remove the CLI:
```
pip uninstall globus-cli
```
More information about Globus CLI Reference: https://docs.globus.org/cli/reference/

# Setup  
First setup the directory structure and clone the package 
```
mkdir NetBASILISK 
cd NetBASILISK 
git clone ssh://git@gitlab.cern.ch:7999/jguhit/netbasilisk.git
```
**Note:** The benchmark script already does the setup for the ATLAS environment and subsequent packages. 

# Running Executables 
<img src="images/diagram.png" width=400 text-align:center ALIGN="left" />  
 <br/> <br/>  <br/> <br/>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   main.sh: Runs run_control.sh in the background <br/> <br/>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   run_control.sh: Runs benchmark.sh followed by cleanup.sh N times to <br/>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   understand and get statistics of benchmark.sh behavior  <br/> <br/>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   atlas_setup.sh: Environment setup, access to rucio, and voms proxy  <br/> <br/>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   benchmark.sh Script responsible for  transferring specific set of files <br/> 
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   and timing how long it takes. Produces log files  <br/> <br/>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   cleanup.sh: Cleans up the directory of transferred files to prepare for <br/>  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   new transfer  <br/> <br/>  <br/> <br/>  <br/> <br/> 

To run (having setup the environment), save the log files to different directories 
```
mkdir main run_control benchmark
```
To run the script in background, run the following command: 
```
nohup bash main.sh &>/dev/null &
```
# Contact

**Jem Guhit**: Graduate Student, Physics. University of Michigan. guhitj@umich.edu <br/>
**Shawn McKee**: Research Scientist. University of Michigan. smckee@umich.edu
