#!/bin/bash

#****************************************#
#              cleanup.sh
#          written by Jem Guhit
#             July 23, 2020

#          Clean up files before 
#         starting a new transfer
#****************************************#
Output=/lustre/umt3/user/guhitj/Gitlab/Globus/netbasilisk/XRootD/run_control/
currentdate=`date +"%Y%m%d_%H%M"`
#Transferring File
startTot=$(date +%s) 
echo "Starting Benchmark Program" 
for n in {1..10}; do #Number of times you want to repeat the benchmark
   echo "TEST $n"

   #ATLAS SETUP
   ###########################################################
   echo "Reading atlas_setup.sh"
   nohup bash atlas_setup.sh &>/dev/null &
   if [ $? -eq 0 ]; then
    #echo OK
   else
    echo "FAIL. Error Code: $?"
   fi
   ###########################################################
   
   #Benchmark
   ###########################################################
   echo "Reading benchmark.sh"
   #S1 means the number of streams used for the transfer is one. You could modify. 
   nohup benchmark.sh > ${Output}${currentdate}_S1test$n.log 2>&1 &
   if [ $? -eq 0 ]; then
    #echo OK
   else
    echo "FAIL. Error Code: $?" 
   fi

   echo "Wait for xrootd to finish"
   wait

   if [ $? -eq 0 ]; then
    #echo OK
   else
    echo "FAIL. Error Code: $?"
   fi
   ###########################################################
   
   #CLEANUP
   ########################################################### 
   echo "Reading cleanup.sh"
   bash cleanup.sh
   if [ $? -eq 0 ]; then
    #echo OK
   else
    echo "FAIL. Error Code:$?"
   fi
done
endTot=$(date +%s)
secondsTot=$(echo "$endTot - $startTot" | bc)
echo 'Total Benchmark time is: ' $secondsTot 'seconds' 
FileSize= 219885.3914
Bandwidth=$( echo "scale=5 ; $var1/$var2"| bc)
echo "Bandwidth:" $Bandwidth "Mb/s"
echo "------------------------------------------------------------"
echo "Benchmark Program Successful"

