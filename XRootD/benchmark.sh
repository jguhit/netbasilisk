#!/bin/bash

#****************************************#
#              benchmark.sh
#          written by Jem Guhit
#             July 23, 2020

#         Script responsible for  
#       transferring specific set of 
#       files and timing how long it 
#       takes. Produces log files
#****************************************#

#Directory of files 
Source=root://dcgftp.usatlas.bnl.gov:1094//pnfs/usatlas.bnl.gov/MCDISK/hiro/test/

#Directory of transferred files 
Dest=root://xrootd.aglt2.org:1094//pnfs/aglt2.org/atlasscratchdisk/benchmark_xrootd/

#Directory of log files 
Output=/lustre/umt3/user/guhitj/Gitlab/Globus/netbasilisk/XRootD/benchmark/
currentdate=`date +"%Y%m%d_%H%M"`

#Creates the $Dest directory. 
uberftp -mkdir gsiftp://dcdum01.aglt2.org/pnfs/aglt2.org/atlasscratchdisk/benchmark_xrootd

echo "Begin Loop"
mkdir -p ${Output}${currentdate}
if [ $? -eq 0 ]; then
    #echo OK
else
    echo "FAIL. Error Code: $?"
fi
startTot=$(date +%s)
paste TestFiles_medium.txt Checksumvals_medium.txt | while read line1 line2; do
  echo "Reading $line1"
  start=$(date +%s)
 # xrdcp --verbose --debug 3 --force --streams 1 --cksum adler32:$line2 $Source$line1 $Dest$line1 > ${Output}${currentdate}/debug${line1}.log 2>&1 | tee ${Output}${currentdate}/${line}S1.log
  ((xrdcp --verbose --debug 3 --force --streams 1 --cksum adler32:$line2 $Source$line1 $Dest$line1 2>&1 1>&3 | tee ${Output}${currentdate}/debug${line1}_stderr.log) 3>&1 1>&2 | tee  ${Output}${currentdate}/debug${line1}_stdout.log) > ${Output}${currentdate}/debug${line}_all.log 2>&1
  if [ $? -eq 0 ]; then
    #echo OK
  else
    echo "FAIL. Error Code: $?"
  fi
  
  echo "Wait for log files to finish" 
  wait 
   
  #####################################################################
  #The wait command waits for the log files to be created so we could 
  #use it for additional check. Which is to determine if there are 
  #errors that occured in the transfer
  #####################################################################

  if grep "error" ${Output}${currentdate}/debug${line}_stderr.log; then
    echo "An error occurred during globus transfer, see ${Output}${currentdate}/debug${line}P1_stderr.log file. CONTINUE"
    grep "error" ${Output}${currentdate}/debug${line}P1_stderr.log
    continue
  else 
    echo "No errors found, continue"
  fi
  
  end=$(date +%s)
  seconds=$(echo "$end - $start" | bc)
  echo $seconds 'seconds' 
done
endTot=$(date +%s)
secondsTot=$(echo "$endTot - $startTot" | bc)
echo 'Total transfer time is: ' $secondsTot 'seconds'
#FileSize= 219885.3914
#Bandwidth=$( echo "scale=5 ; $var1/$var2"| bc)
#echo "Bandwidth:" $Bandwidth "Mb/s"
echo "Done!"
echo "------------------------------------------------------"

