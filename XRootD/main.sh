#!/bin/bash

#****************************************#
#              main.sh
#          written by Jem Guhit
#             July 23, 2020

#    Runs run_control.sh in background
#****************************************#
/lustre/umt3/user/guhitj/Gitlab/Globus/netbasilisk/XRootD
Output=/lustre/umt3/user/guhitj/Gitlab/Globus/netbasilisk/XRootD/main/
currentdate=`date +"%Y%m%d_%H%M"`

#S1 means number of streams during the transfer was 1, you could modify this number to specify S
nohup bash run_control.sh > ${Output}BenchmarkS1${currentdate}.log 2>&1 &
