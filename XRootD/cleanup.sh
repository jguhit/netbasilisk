#!/bin/bash

#****************************************#
#              cleanup.sh
#          written by Jem Guhit
#             July 23, 2020

#          Clean up files before 
#         starting a new transfer
#****************************************#


echo "Starting cleanup process"
echo "Sleep for 1 min"
sleep 1m
echo "Removing previous directory"
uberftp -rm -r gsiftp://dcdum01.aglt2.org/pnfs/aglt2.org/atlasscratchdisk/benchmark_xrootd 
if [ $? -eq 0 ]; then
    #echo OK
else
    echo "FAIL, Error Code: $?"
fi
echo "Done! Restarting new transfer"
