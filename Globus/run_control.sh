#!/bin/bash

#Directory where log files are produced. MODIFY
Output=/lustre/umt3/user/guhitj/Gitlab/Globus/run_control/
currentdate=`date +"%Y%m%d_%H%M"`
startTot=$(date +%s) 
echo "Starting Benchmark Program" 
for n in {1..2}; do   #Number of times you want to repeat the benchmark e.g 5, 10. 
   echo "TEST $n"
   ###########################################################
   #Comment out atlas_setup.sh if you do not need to setup 
   #the ATLAS environment and access rucio and the voms proxy
   ###########################################################
  
   #ATLAS SETUP
   ###########################################################
   #echo "Reading atlas_setup.sh"
   #nohup bash atlas_setup.sh &>/dev/null &
   #if [ $? -eq 0 ]; then
    #echo OK
   #else
   # echo FAIL
   #fi
   ###########################################################

   #BENCHMARK
   ###########################################################
   echo "Reading benchmark.sh"
   nohup bash benchmark.sh > ${Output}${currentdate}_P1test$n.log 2>&1 &
   if [ $? -eq 0 ]; then
    #echo OK
   else
    echo "FAIL. Error Code: $?"
   fi
   echo "Wait for globus to finish"
   wait
   if [ $? -eq 0 ]; then
    #echo OK
   else
    echo "FAIL. Error Code: $?"
   fi
   ###########################################################
   
   #CLEANUP
   ###########################################################
   echo "Reading cleanup.sh"
   bash cleanup.sh
   if [ $? -eq 0 ]; then
    #echo OK
   else
    echo "FAIL. Error Code: $?"
   fi
   ###########################################################
done
endTot=$(date +%s)
secondsTot=$(echo "$endTot - $startTot" | bc)
echo 'Total Benchmark time is: ' $secondsTot 'seconds' 
echo "Benchmark Program Successful"

