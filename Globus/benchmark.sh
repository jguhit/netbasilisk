#!/bin/bash

#Directory of test files. MODIFY 
#Syntax: SOURCE_ENDPOINT_ID:SOURCE_PATH
Source=gsiftp://dcgftp.usatlas.bnl.gov/pnfs/usatlas.bnl.gov/MCDISK/hiro/test/

#Directory of where the transferred files go. MODIFYi
#Syntax: DEST_ENDPOINT_ID:DEST_PATH
Dest=gsiftp://dcdum01.aglt2.org/pnfs/aglt2.org/atlasscratchdisk/benchmark_globus/

#Directory of log files. MODIFY
Output=/lustre/umt3/user/guhitj/Gitlab/Globus/benchmark/
currentdate=`date +"%Y%m%d_%H%M"`

#Txt file containing the names of the files you want to transfer. MODIFY
Filename='TestFiles.txt'

#Creates the $Dest directory. MODIFY
#uberftp -mkdir gsiftp://dcdum01.aglt2.org/pnfs/aglt2.org/atlasscratchdisk/benchmark_globus

#Use the following syntax to create directory. For more syntax, check: https://docs.globus.org/cli/reference/mkdir/
#globus mkdir ENDPOINT:PATH

echo "Begin Loop"
mkdir -p ${Output}${currentdate}
if [ $? -eq 0 ]; then
    #echo OK
else
    echo "FAIL, Problems with creating Directory. Error Code: $?"
fi
startTot=$(date +%s)
while IFS= read -r line; do
  echo "Reading $line"
  start=$(date +%s)

  #####################################################################
  #For more info about the command and the options, check 
  #https://docs.globus.org/cli/reference/transfer/
  #The syntax saves the stdout, stderr, and both to different log files
  #####################################################################
   
  ((globus transfer -vvv --verify-checksum --checksum-algorithm adler32 $Source$line $Dest$line 2>&1 1>&3| tee ${Output}${currentdate}/debug${line}P1_stderr.log) 3>&1 1>&2 | tee ${Output}${currentdate}/debug${line}P1_stdout.log) > ${Output}${currentdate}/debug${line}P1_all.log 2>&1
  if [ $? -eq 0 ]; then
    #echo OK
  else
    echo "FAIL, Problem with running globus. Error Code: $?"
  fi
  echo "Wait for log files to finish" 
  wait 
  
  #####################################################################
  #The wait command waits for the log files to be created so we could 
  #use it for additional check. Which is to determine if there are 
  #errors that occured in the transfer
  #####################################################################

  if grep "error" ${Output}${currentdate}/debug${line}P1_stderr.log; then
    echo "An error occurred during globus transfer, see ${Output}${currentdate}/debug${line}P1_stderr.log file"
    grep "error" ${Output}${currentdate}/debug${line}P1_stderr.log
    echo "Error, but continue"
    continue
  else 
    echo "No errors found, continue"
  fi
  end=$(date +%s)
  seconds=$(echo "$end - $start" | bc)
  echo $seconds 'seconds' 
done < "$Filename"
endTot=$(date +%s)
secondsTot=$(echo "$endTot - $startTot" | bc)
echo 'Total transfer time is: ' $secondsTot 'seconds' 
echo "Done!"
