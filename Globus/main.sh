#!/bin/bash

#Location of the log file. Modify to your specific directory
Output=/lustre/umt3/user/guhitj/Gitlab/Globus/main/ 
currentdate=`date +"%Y%m%d_%H%M"`

nohup bash run_control.sh > ${Output}${currentdate}_BenchmarkP1.log 2>&1 &
