#!/bin/bash

echo "Starting cleanup process"
echo "Sleep for 1 min"
sleep 1m
echo "Removing previous directory"
#Removing the $Dest directory before starting new transfer. MODIFY
#uberftp -rm -r gsiftp://dcdum01.aglt2.org/pnfs/aglt2.org/atlasscratchdisk/benchmark_globus

#Check https://docs.globus.org/cli/reference/rm/ for more documentation on globus rm
globus rm <ENDPOINT_ID:PATH> --recursive 
if [ $? -eq 0 ]; then
    #echo OK
else
    echo "FAIL. Error Code $?"
fi
echo "Done! Restarting new transfer"
